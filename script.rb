#!/usr/bin/env ruby

require_relative 'opcodes'
require_relative 'decode'
require_relative 'encode'

# first command line input is the command (either decode or encode)
command = ARGV[0]
# second command line input is the input
# for decode, the input should be a hex string. e.g.: "76a9143647ceb2fdec47d3594c6653eda85e534794a49a88ac"
# for encode, the input should be opcodes and data. e.g.: "OP_DUP OP_HASH160 fda257aea99a23b389f26d235797aacbab556f9e OP_EQUALVERIFY OP_CHECKSIG"
input = ARGV[1]

case command
	when "decode"
		decode(input)
	when "encode"
		encode(input)
	else
		puts "unkown command :("
end




